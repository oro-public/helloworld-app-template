> This project template can be used for creating custom apps. This is a complete react project created with create-react-app tool. All the apps will be created under 'src' folder.

#### App Directory Structure Explanation
- app-name: app name for your app.
app-name must be same as mentioned while uploading an app. otherwise app won't run.
- components: This directory will have all the react components for your app.
- routes.js: this file will have an array of all the routes with keys - path, components
    - path - any route for your app. your path will get prefix "<app-name>/"  
      e.g. If you enter path value as "my-route", your final route in browser will look as "<app-name>/my-route". 
      so while implementing navigation in your app always use final url (<app-name>/my-route) in href value
    - component - component will hold link to the imported component that you want to show on mentioned route.
- package.json: this file will have all the dependencies required for your app. Copy all the app specific dependencies from './package.json' to './src/app/package.json'. Do not include react, react-dom, react-scripts or any other core react libraries.

#### How to write your app?
- Create app folder with above explained structure inside 'src/apps'.
- Write your app routes in routes.js inside your app folder.
- Write react components for your app inside 'components; folder.
- Copy all dependencies from './package.json' to './src/apps/<your-app>/package.json'

#### Things to know.
- How to authenticate backend APIs?
  - In './App.js', we have called a login api using axios. On successfull login, we are storing auth token in storage.
  - Always use `window.axiosIns` to call any API. Doing that will automatically append authentication token to your request and you do not need to worry about authentication.
- How to navigate between app routes?
  - You have to use react-router-dom for implementing navigation. eg. <Link> or history.push('/')
  - Make sure all your routes have 'app-name' prefex while navigating. eg. /app-one/component1
  - Do not add prefix in routes.js. (we will append it automatically after app installation.)
- How to style your app components?
  - This project is pre-installed with material-ui.
  - You can use all the material-ui components to develop & style your app.

#### How to deploy app?
- Once your app is developed and tested, make zip of your app content. eg. make zip of contents of './src/apps/app_one'
- Login to super admin and navigate to 'Apps' from sidebar.
- Click on 'Add' to add new app and fill all the required details and upload zip that we just created.
- Once uploaded, Select created app from the list and click on 'Activate' & 'Deploy' respectively.
- Within few minutes your app will be deployed and will be available for customer admins to install in App Store area.

#### How to run this project?
```sh
$ cd orchestration-frontend-app-template
$ npm install
$ npm start
```