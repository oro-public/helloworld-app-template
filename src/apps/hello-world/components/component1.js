import React, { useEffect } from 'react';
import { Link } from 'react-router-dom'

const ComponentOne = (props) => {
    useEffect(() => {
        // axios example. always use "window.axiosIns" for API calls.
        window.axiosIns.get('https://jsonplaceholder.typicode.com/todos/1').then((value) => {
            debugger;
        }, [])
    })

    return (
        <div style={{padding: 10}}>
            <span style={{fontSize: 35}}>Hello World</span>
        </div>
    )
}

export default ComponentOne;