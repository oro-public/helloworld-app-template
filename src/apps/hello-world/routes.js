import ComponentOne from "./components/component1";

const Routes = [
    {
        path: "/",
        component: ComponentOne,
    },
    {
        path: "/hello-world",
        component: ComponentOne,
    }
];

export default Routes;
