import React, { Component } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import './index.scss';
import { AppBar } from '@material-ui/core';

const useStyles = makeStyles(theme => ({
    drawer: {
        width: 80,
        borderRight: '1px solid #b1b1b1',
        zIndex: 1,
    },
    appbar: {
        boxShadow: 'none',
        background: "#fff",
        height: 64,
        borderBottom: '1px solid #b1b1b1',
    },
}));

const Layout = (props) => {
    const classes = useStyles();

    return (
        <div id="layout-wrapper">
            <div className={classes.root}>
                <AppBar position="fixed" className={classes.appbar}>
                    appbar
                </AppBar>
            </div>
            <div>
                <Drawer
                    variant={"permanent"}
                    classes={{ paper: classes.drawer }}
                    open
                >
                    aa
                    </Drawer>
            </div >
            <div className="content-wrapper">
                {React.cloneElement(props.children, {})}
            </div>
        </div>
    )
}

export default Layout;
